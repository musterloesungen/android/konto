# Details zum AndroidManifest.xml

Das `AndroidManifest.xml` spielt eine zentrale Rolle in der Konfiguration der Android Banking App. Dieses Dokument erläutert die spezifischen Einstellungen und Berechtigungen, die im Manifest festgelegt sind, sowie ihre Bedeutung für die Funktionalität und Sicherheit der App.

## Struktur und Schlüsselkomponenten

### Grundlegende Konfiguration

- **Package-Name**: Dient als eindeutige Identifikation der App im Android-System und auf dem Google Play Store.
- **Zugriffsrechte (Permissions)**: Definiert, welche Systemressourcen die App nutzen darf. In diesem speziellen Fall werden keine spezifischen Berechtigungen benötigt, da die App keine Hardwarefunktionen wie GPS oder Kamera nutzt und keine sensiblen persönlichen Daten abfragt.

### Application-Tag

Dieser Abschnitt definiert globale Eigenschaften der gesamten Anwendung:

- **android:allowBackup**: Erlaubt es, dass Benutzerdaten automatisch über Google Drive gesichert und wiederhergestellt werden können.
- **android:fullBackupContent**: Referenziert eine XML-Datei, die definiert, welche Daten gesichert werden sollen und welche nicht.
- **android:icon** und **android:roundIcon**: Legen die Icons fest, die die App auf dem Gerät und im Launcher repräsentieren.
- **android:label**: Der Name der App, wie er dem Benutzer angezeigt wird.
- **android:supportsRtl**: Unterstützung für Sprachen, die von rechts nach links gelesen werden.
- **android:theme**: Definiert das visuelle Erscheinungsbild der App durch Verweis auf ein spezifisches Theme.

### Activity-Konfigurationen

- **android:name**: Gibt den Namen der `MainActivity` an, die als Einstiegspunkt für die App dient.
- **android:exported**: Muss auf `true` gesetzt sein, damit die Activity von anderen Apps oder dem System gestartet werden kann, insbesondere da sie einen `intent-filter` enthält.

### Intent-Filter

- **android.intent.action.MAIN** und **android.intent.category.LAUNCHER**: Diese Einstellungen legen fest, dass `MainActivity` der Einstiegspunkt der App ist, also die erste UI-Komponente, die beim Starten der App angezeigt wird.

## Sicherheitsaspekte

Die Sicherheitseinstellungen im Manifest sind kritisch für den Schutz der App und ihrer Daten:

- **android:exported**: Stellt sicher, dass die `MainActivity` sicher von anderen Komponenten gestartet werden kann. Für Komponenten, die nicht von außerhalb der App gestartet werden sollen, sollte dieser Wert auf `false` gesetzt werden, um zu verhindern, dass andere Apps oder das System die Komponenten direkt aufrufen können.
