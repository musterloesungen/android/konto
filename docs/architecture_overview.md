# Überblick über die Architektur

Dieses Dokument bietet einen detaillierten Überblick über die Architektur der Android Banking App. Es erklärt die wichtigsten Komponenten der App, ihre Interaktionen und wie sie zusammenwirken, um die Funktionalität der App bereitzustellen.

```mermaid
graph TD;
    MainActivity[MainActivity] --> UIComponents[UI Components]
    MainActivity --> DataModels[Data Models]
    UIComponents --> TextViews[TextViews]
    UIComponents --> EditTexts[EditTexts]
    UIComponents --> Buttons[Buttons]
    UIComponents --> TableLayout[TableLayout]
    DataModels --> Konto[Konto]
    DataModels --> Girokonto[Girokonto]
    Konto --> Transaktionsmethoden[Transaktionsmethoden]
    Girokonto --> Transaktionsmethoden
    MainActivity --> ActivityLifecycle[Activity Lifecycle]
    ActivityLifecycle --> onCreate[onCreate]
    ActivityLifecycle --> onStart[onStart]
    ActivityLifecycle --> onResume[onResume]
    ActivityLifecycle --> onPause[onPause]
    ActivityLifecycle --> onStop[onStop]
    ActivityLifecycle --> onDestroy[onDestroy]

    classDef default fill:#f9f,stroke:#333,stroke-width:2px;
    class MainActivity,UIComponents,DataModels,ActivityLifecycle default;
    class TextViews,EditTexts,Buttons,TableLayout,Konto,Girokonto,Transaktionsmethoden,onCreate,onStart,onResume,onPause,onStop,onDestroy default;
```

## Modulare Struktur

Die App ist modular aufgebaut, um Wartbarkeit, Testbarkeit und Skalierbarkeit zu verbessern. Die Hauptmodule umfassen:

- **UI-Komponenten**: Diese umfassen alle Views und Aktivitäten, die für die Benutzerinteraktion verantwortlich sind.
- **Datenmodelle**: Klassen, die die Geschäftslogik und die Datenstruktur repräsentieren, wie `Konto` und `Girokonto`.
- **Service-Schicht**: Beinhaltet Geschäftslogik, die nicht direkt mit der UI verbunden ist, wie Transaktionsmanagement.
- **Datenzugriffsschicht**: Verwaltet die Datenpersistenz und den Zugriff auf externe Datenquellen.

## Hauptkomponenten

### UI-Komponenten

- **MainActivity**: Die Hauptaktivität, die als Einstiegspunkt für die Benutzer dient. Sie ist verantwortlich für das Rendering der Benutzeroberfläche und die Handhabung von Benutzerinteraktionen.
- **Layouts**: Definiert in XML, verwenden `RelativeLayout` und `LinearLayout`, um flexible und anpassungsfähige Benutzeroberflächen zu gestalten.
- **Views**: Elemente wie `TextView`, `EditText`, `Button` und `TableLayout` werden genutzt, um Informationen darzustellen und Interaktionen zu ermöglichen.

### Datenmodelle

- **Konto**: Eine abstrakte Basisklasse, die gemeinsame Eigenschaften und Methoden für alle Kontoarten definiert.
- **Girokonto**: Erbt von `Konto` und fügt spezifische Eigenschaften wie einen Dispositionskredit hinzu.

### Geschäftslogik

- **Transaktionsmanagement**: Methoden in den Klassen `Konto` und `Girokonto` ermöglichen es, Geld ein- und auszuzahlen und den Kontostand zu überprüfen.
- **Validierungen**: Stellen sicher, dass nur gültige Operationen durchgeführt werden (z.B. keine negativen Beträge).

### Datenzugriffsschicht

Obwohl in der Basisimplementierung möglicherweise nicht enthalten, würde eine erweiterte Version eine Datenzugriffsschicht beinhalten, um:

- **Datenpersistenz**: Speichern von Kontoinformationen in einer lokalen Datenbank oder über externe APIs.
- **Datenabruf**: Laden von Kontodaten bei App-Start oder bei Bedarf.

## Technologien und Frameworks

- **Android SDK**: Bietet die notwendigen Tools und APIs zur Entwicklung der App.
- **Kotlin**: Als Hauptprogrammiersprache für die Entwicklung.
- **Android Studio**: Als integrierte Entwicklungsumgebung (IDE) zur Vereinfachung von Entwicklung, Debugging und Testing.

## Sicherheitsaspekte

- **Manifest-Konfigurationen**: Definieren von Sicherheitseinstellungen wie `android:exported`.
- **Datenhandling**: Sicheres Verwalten von Nutzereingaben und Transaktionsdaten.
