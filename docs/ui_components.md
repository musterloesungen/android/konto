# UI-Komponenten

Dieses Dokument bietet eine detaillierte Beschreibung der Benutzeroberflächenkomponenten der Android Banking App. Es erläutert die Rolle und Funktionalität jeder UI-Komponente innerhalb der `MainActivity` und wie diese zum Gesamterlebnis der App beitragen.

## Layouts und Views

Die App verwendet `RelativeLayout` als Hauptlayout, um eine flexible Anordnung der UI-Elemente zu ermöglichen. Dieses Layout wird ergänzt durch `LinearLayout`, das zur vertikalen Anordnung von UI-Komponenten innerhalb der `CardView` verwendet wird.

### CardView

- **ID**: `cardViewKontodetails`
- **Beschreibung**: Dient als Container für die Kontodetails. Bietet visuelle Trennung durch Karten-Design mit Schattierung und abgerundeten Ecken.
- **Eigenschaften**: Einstellungen wie `cardCornerRadius` und `cardElevation` verbessern die Ästhetik und Lesbarkeit.

### TextViews

- **IDs**: `tvKontostand`, `tvDispo`
- **Beschreibung**: Zeigen Informationen wie den aktuellen Kontostand und den verfügbaren Dispo an.
- **Eigenschaften**: Textgröße (`textSize`), Schriftart und Margins sind so gewählt, dass sie gut lesbar und optisch ansprechend sind.

### EditText

- **ID**: `etBetrag`
- **Beschreibung**: Ermöglicht dem Benutzer, einen Betrag für Transaktionen einzugeben.
- **Eigenschaften**: `inputType="numberDecimal"` stellt sicher, dass nur numerische und dezimale Eingaben möglich sind.

### Buttons

- **IDs**: `btnEinzahlen`, `btnAuszahlen`
- **Beschreibung**: Ermöglichen das Einzahlen und Auszahlen von Beträgen.
- **Funktionalität**: Verknüpft mit Event-Handlern, die die entsprechenden Methoden in der Kontenlogik aufrufen.

## TableLayout

- **ID**: `tableKonten`
- **Beschreibung**: Stellt eine Liste von Konten dar, wobei jedes Konto in einer Zeile angezeigt wird.
- **Eigenschaften**: Dynamische Hinzufügung von `TableRow`-Elementen basierend auf den verfügbaren Konten.

### TableRow

- **Beschreibung**: Jede Zeile enthält Details zu einem spezifischen Konto.
- **Interaktion**: Klickbare Zeilen, die den Kontodetailbereich aktualisieren, wenn ein bestimmtes Konto ausgewählt wird.

## Snackbar und Toast-Nachrichten

- **Beschreibung**: Snackbar und Toast-Nachrichten werden verwendet, um Feedback und Bestätigungen für Benutzeraktionen wie erfolgreiche Transaktionen oder Fehlermeldungen zu geben.
- **Einsatz**: Snackbars bieten eine visuelle und interaktive Feedback-Option am unteren Bildschirmrand, während Toasts kurzlebige Nachrichten sind, die weniger interaktiv sind.

## Stil und Design

- **Thema**: Die App verwendet ein benutzerdefiniertes Theme (`@style/Theme.Konto`), das die Farben, Schriftarten und andere visuelle Aspekte der gesamten App definiert.
- **Farben und Icons**: Die Farbpalette und die Icons sind so gewählt, dass sie eine klare, professionelle und freundliche Benutzererfahrung bieten.
