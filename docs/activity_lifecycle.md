# Lebenszyklus der MainActivity

In diesem Dokument wird der Lebenszyklus der `MainActivity` der Android Banking App erläutert. Es bietet Einblicke in die verschiedenen Zustände, durch die eine Activity geht, und beschreibt, wie `MainActivity` diese Zustände für eine optimale Benutzererfahrung und effiziente Ressourcennutzung handhabt.

## Grundlagen des Activity-Lebenszyklus

Eine Android-Activity durchläuft verschiedene Zustände in ihrem Lebenszyklus, von ihrer Erstellung bis zur Zerstörung. Die Hauptmethoden des Lebenszyklus sind:

- **onCreate()**: Wird aufgerufen, wenn die Activity zum ersten Mal erstellt wird. Hier werden das Layout initialisiert und die UI-Komponenten gebunden.
- **onStart()**: Wird aufgerufen, wenn die Activity für den Benutzer sichtbar wird.
- **onResume()**: Wird aufgerufen, wenn die Activity anfängt, mit dem Benutzer zu interagieren. Zu diesem Zeitpunkt ist die Activity im Vordergrund und aktiv.
- **onPause()**: Wird aufgerufen, wenn das System eine andere Activity startet und diese Activity nicht mehr im Vordergrund ist. Sie kann aber immer noch sichtbar sein.
- **onStop()**: Wird aufgerufen, wenn die Activity nicht mehr sichtbar ist.
- **onDestroy()**: Wird aufgerufen, bevor die Activity zerstört wird. Hier sollten alle Ressourcen freigegeben werden.

## Implementierung in `MainActivity`

### onCreate(Bundle savedInstanceState)

- **Layout und UI-Komponenten**: `MainActivity` lädt das XML-Layout und initialisiert die UI-Komponenten wie Buttons, TextViews und das TableLayout.
- **Dateninitialisierung**: Instanzen von `Girokonto` werden erstellt und in einer Liste gespeichert. Diese Initialisierung simuliert eine Datenbank oder eine externe Datenquelle.

### onStart()

- In `MainActivity` wird diese Methode genutzt, um sicherzustellen, dass alle benötigten Ressourcen verfügbar sind und die UI korrekt konfiguriert ist.

### onResume()

- Die App reagiert auf Benutzereingaben und aktualisiert die UI entsprechend den aktuellen Daten der Konten. Event-Listener für Buttons und andere interaktive Elemente werden in dieser Phase vollständig aktiviert.

### onPause()

- Änderungen an den Daten oder der UI, die gespeichert werden müssen, sollten hier commitet werden, da die Activity jederzeit beendet werden könnte.

### onStop()

- `MainActivity` nutzt `onStop()`, um Netzwerkverbindungen oder Datenbankverbindungen zu trennen, die nicht mehr benötigt werden, wenn die App nicht sichtbar ist.

### onDestroy()

- Hier werden alle Ressourcen freigegeben, insbesondere solche, die viel Speicher verbrauchen. Auch die Listener und temporäre Datenstrukturen werden bereinigt, um Lecks zu vermeiden.
