# Datenverwaltung

Dieses Dokument beschreibt, wie die Android Banking App Daten handhabt, speichert und manipuliert. Es umfasst Informationen über die Speicherung von Benutzerdaten, die Verwaltung von Transaktionen und die Sicherheit bei der Datenverarbeitung.

## Grundlegende Datenstrukturen

Die App verwendet vorrangig zwei Klassen zur Datenverwaltung:

- **Konto**: Diese Klasse stellt die Grundlage für alle Kontoarten dar. Sie speichert den Kontostand und bietet grundlegende Methoden wie `einzahlen`, `auszahlen` und `kontostand`.
- **Girokonto**: Eine Erweiterung der `Konto`-Klasse, die zusätzlich einen Dispositionskredit (Dispo) verwaltet. Die Klasse ermöglicht erweiterte Transaktionsmethoden, die den Dispo berücksichtigen.

## Datenfluss

### Benutzerinteraktionen

Benutzerinteraktionen, die zu Datenänderungen führen (wie das Einzahlen und Auszahlen von Geld), werden durch die Benutzeroberfläche initiiert. Einzelheiten zu diesen Interaktionen umfassen:

- **Einzahlungen**: Der Benutzer gibt einen Betrag in ein `EditText`-Feld ein. Nach der Bestätigung wird der Betrag über die `einzahlen`-Methode zum Kontostand hinzugefügt.
- **Auszahlungen**: Ähnlich wie bei Einzahlungen gibt der Benutzer einen Betrag ein. Die `auszahlen`-Methode wird aufgerufen, und es wird überprüft, ob der Auszahlungsbetrag verfügbar ist, unter Berücksichtigung des Dispos.

### Datenaktualisierung und Anzeige

- **Aktualisierung der Kontodetails**: Nach jeder Transaktion werden die Kontodetails in der Benutzeroberfläche aktualisiert, um den neuen Kontostand und ggf. den neuen Dispo anzuzeigen.
- **Tabellenansicht**: Die `TableLayout`-Komponente zeigt eine Liste aller Konten an. Diese Liste wird aktualisiert, um Änderungen in den Kontoständen widerzuspiegeln.

## Datenpersistenz

Obwohl das Basisbeispiel möglicherweise keine tatsächliche Datenbank verwendet, würde eine vollständige Implementierung die Nutzung einer lokalen Datenbank wie SQLite oder eine Remote-Datenbank über eine API beinhalten, um:

- **Dauerhafte Speicherung**: Kontostände und Dispositionskredite werden dauerhaft gespeichert, um bei Neustart der App oder nach Updates der App-Daten verfügbar zu sein.
- **Sicherheit und Integrität**: Durch die Verwendung gesicherter Verbindungen und geeigneter Verschlüsselungsmethoden werden die Daten geschützt.

## Sicherheit

Die Sicherheit der Datenverarbeitung wird durch verschiedene Mechanismen gewährleistet:

- **Validierung von Eingaben**: Alle Benutzereingaben werden auf Gültigkeit und Zulässigkeit überprüft, um sicherzustellen, dass nur korrekte und realisierbare Transaktionen durchgeführt werden.
- **Verschlüsselung**: Sensible Daten wie Kontostände und Transaktionsdetails sollten verschlüsselt gespeichert werden, um sie vor unbefugtem Zugriff zu schützen.
