# Klassenbeschreibungen

In diesem Dokument werden die zentralen Klassen der Android Banking App beschrieben, welche die Datenmodelle `Konto` und `Girokonto` umfassen. Diese Klassen implementieren die Kernfunktionalitäten der App, wie das Verwalten von Konten und das Durchführen von Finanztransaktionen.

## Konto

### Beschreibung

Die Klasse `Konto` dient als abstrakte Basisklasse für verschiedene Arten von Bankkonten. Sie definiert allgemeine Eigenschaften und Methoden, die für alle Kontotypen gelten.

### Attribute

- `saldo`: Ein `Double`, das den aktuellen Saldo des Kontos hält. Dieses Attribut ist als `protected` deklariert, sodass es nur innerhalb der Klasse und von abgeleiteten Klassen aus zugänglich ist.

### Methoden

- **einzahlen(betrag: Double)**: Ermöglicht es, einen positiven Betrag auf das Konto einzuzahlen. Der Betrag wird zum aktuellen Saldo hinzugefügt, sofern er größer als Null ist.
- **auszahlen(betrag: Double): Boolean**: Versucht, einen Betrag vom Konto abzuziehen. Gibt `true` zurück, wenn der Betrag positiv ist und der Saldo ausreichend ist, sonst `false`.
- **kontostand(): Double**: Gibt den aktuellen Saldo des Kontos zurück.

## Girokonto

### Beschreibung

`Girokonto` ist eine konkrete Klasse, die von `Konto` erbt und spezifische Funktionen für ein Girokonto bereitstellt, einschließlich der Verwaltung eines Dispositionskredits.

### Attribute

- `dispo`: Ein `Double`, das den maximal verfügbaren Dispositionskredit beschreibt. Der Zugriff auf dieses Attribut ist auf die Klasse `Girokonto` und deren Methoden beschränkt (private Sichtbarkeit).

### Methoden

- **auszahlen(betrag: Double): Boolean**: Überschreibt die `auszahlen`-Methode der Basisklasse `Konto`. Diese Methode ermöglicht es, Beträge auszuzahlen, die den aktuellen Saldo überschreiten, solange der Dispositionskredit dies zulässt. Gibt `true` zurück, wenn die Summe aus Saldo und Dispo ausreicht, um den Betrag zu decken.
- **setDispo(neuerDispo: Double)**: Setzt den Dispositionskredit neu. Akzeptiert nur positive Werte oder Null.
- **getDispo(): Double**: Gibt den aktuellen Dispositionskredit zurück.

## Nutzung der Klassen in der App

Die Klassen `Konto` und `Girokonto` werden verwendet, um die finanziellen Aspekte der Benutzerinteraktion zu verwalten. Benutzer können über die Benutzeroberfläche Geld einzahlen und abheben. Die entsprechenden Methoden dieser Klassen werden aufgerufen, um die korrekten Beträge zu verarbeiten und den aktuellen Kontostand oder den Dispositionskredit zu aktualisieren.
