# Setup-Anweisungen

Diese Anleitung führt Sie durch die Schritte zur Einrichtung der Entwicklungsumgebung für die Android Banking App. Hierbei werden alle notwendigen Softwareinstallationen und Konfigurationen beschrieben, die für die effektive Entwicklung und das Testen der App erforderlich sind.

## Voraussetzungen

Bevor Sie beginnen, stellen Sie sicher, dass Ihr Computer die folgenden Voraussetzungen erfüllt:

- **Betriebssystem**: Windows 10/8/7, Mac OS X 10.8 oder höher, oder eine aktuelle Linux-Distribution
- **RAM**: Mindestens 8 GB, empfohlen sind 16 GB oder mehr für eine flüssigere Ausführung
- **Festplattenspeicher**: Mindestens 4 GB freier Speicherplatz für Android Studio und Android SDK

## Softwareinstallationen

1. **Java Development Kit (JDK)**
    - Laden Sie das JDK von der [Oracle-Website](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) herunter und installieren Sie es.
    - Setzen Sie die Umgebungsvariable `JAVA_HOME`, um auf das JDK-Verzeichnis zu verweisen.

2. **Android Studio**
    - Besuchen Sie die [offizielle Android Studio-Seite](https://developer.android.com/studio) und laden Sie die neueste Version von Android Studio herunter.
    - Führen Sie das Installationsprogramm aus und folgen Sie den Anweisungen zur Installation von Android Studio und den zugehörigen SDK-Tools.

3. **Android SDK**
    - Während der Installation von Android Studio können Sie das Android SDK installieren. Stellen Sie sicher, dass Sie die neueste Android SDK Plattform und die Android SDK Build-Tools installieren.

4. **Android Virtual Device (AVD)**
    - Starten Sie Android Studio und öffnen Sie den AVD Manager, um ein virtuelles Gerät zu erstellen. Wählen Sie ein Gerätemodell und eine Android-Version, die die API-Level-Anforderungen der App erfüllt.

## Konfiguration der Entwicklungsumgebung

Nachdem Sie die benötigte Software installiert haben, führen Sie die folgenden Schritte durch, um Ihre Entwicklungsumgebung einzurichten:

1. **Öffnen Sie Android Studio und importieren Sie das Projekt**
    - Starten Sie Android Studio.
    - Wählen Sie "Open an existing Android Studio project" und navigieren Sie zum geklonten Repository.

2. **Warten auf den Abschluss der Gradle-Synchronisation**
    - Android Studio führt automatisch eine Synchronisation mit den Gradle-Skripten des Projekts durch. Dies kann einige Minuten dauern, je nach Geschwindigkeit Ihrer Internetverbindung und Ihres Computers.

3. **Konfigurieren Sie das SDK**
    - Stellen Sie sicher, dass das korrekte Android SDK und die korrekten Build-Tools in den Projekteinstellungen unter "File > Project Structure > SDK Location" ausgewählt sind.

4. **Lauf der App**
    - Konfigurieren Sie ein AVD oder schließen Sie ein physisches Android-Gerät an Ihren Computer an.
    - Klicken Sie auf das "Run"-Symbol in der Symbolleiste oder drücken Sie `Shift + F10`, um die App zu starten.

Durch die Befolgung dieser Anweisungen sollte Ihre Entwicklungsumgebung vollständig eingerichtet sein und bereit für die Arbeit an der Android Banking App.