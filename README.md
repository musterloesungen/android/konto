# Android Banking App Schulungsprojekt

Dieses Repository enthält den Quellcode und die Dokumentation für eine Android Banking App, die für Schulungszwecke entwickelt wurde. Die App bietet eine Benutzeroberfläche zur Verwaltung von Girokonten, einschließlich Funktionen zum Einzahlen und Abheben von Beträgen sowie zur Anzeige von Kontoständen und Dispositionskrediten.

## Ziel des Projekts

Das Ziel dieses Projekts ist es, angehenden Android-Entwicklern praktische Erfahrungen im Umgang mit Android-Studio, der Kotlin-Programmiersprache und den Grundlagen der Android-App-Entwicklung zu vermitteln.

## Voraussetzungen

Um mit dem Projekt arbeiten zu können, benötigen Sie:
- Android Studio 4.0 oder höher
- JDK 8 oder höher
- Ein Android-Gerät oder Emulator mit Android API Level 21 oder höher

## Installation und Einrichtung

1. Klonen Sie dieses Repository auf Ihren lokalen Computer:
   ```bash
   git clone https://example.com/android-banking-app.git
   ```
2. Öffnen Sie das Projekt in Android Studio.
3. Warten Sie, bis Android Studio alle notwendigen Gradle-Abhängigkeiten heruntergeladen und das Projekt synchronisiert hat.
4. Starten Sie die App über den `Run`-Button in Android Studio auf einem verbundenen Android-Gerät oder einem Emulator.

## Struktur der Dokumentation

Detaillierte Informationen zu den verschiedenen Aspekten der App finden Sie in den folgenden Dokumenten im `docs/` Ordner:

- [Setup-Anweisungen](docs/setup.md)
- [Überblick über die Architektur](docs/architecture_overview.md)
- [Klassenbeschreibungen](docs/class_descriptions.md)
- [Lebenszyklus der Hauptaktivität](docs/activity_lifecycle.md)
- [UI-Komponenten](docs/ui_components.md)
- [Datenverwaltung](docs/data_handling.md)
- [Details zum Manifest](docs/manifest_details.md)
- [Leitfaden zur Bereitstellung](docs/deployment_guide.md)
