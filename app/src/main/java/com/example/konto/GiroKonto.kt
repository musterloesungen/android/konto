package com.example.konto

class Girokonto(saldo: Double, private var dispo: Double) : Konto(saldo) {

    override fun auszahlen(betrag: Double): Boolean {
        return if (betrag > 0 && (saldo + dispo) >= betrag) {
            saldo -= betrag
            true
        } else {
            false
        }
    }

    fun setDispo(neuerDispo: Double) {
        if (neuerDispo >= 0) {
            dispo = neuerDispo
        }
    }

    fun getDispo(): Double {
        return dispo
    }
}