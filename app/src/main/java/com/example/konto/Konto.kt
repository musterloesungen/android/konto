package com.example.konto

open class Konto(protected var saldo: Double) {

    fun einzahlen(betrag: Double) {
        if (betrag > 0) {
            saldo += betrag
        }
    }

    open fun auszahlen(betrag: Double): Boolean {
        return if (betrag > 0 && saldo >= betrag) {
            saldo -= betrag
            true
        } else {
            false
        }
    }

    fun kontostand(): Double {
        return saldo
    }
}