package com.example.konto

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.EditText
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private lateinit var tvKontostand: TextView
    private lateinit var tvDispo: TextView
    private lateinit var etBetrag: EditText
    private var aktuellesKonto: Girokonto? = null
    private val konten = mutableListOf<Girokonto>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvKontostand = findViewById(R.id.tvKontostand)
        tvDispo = findViewById(R.id.tvDispo)
        etBetrag = findViewById(R.id.etBetrag)
        val btnEinzahlen: Button = findViewById(R.id.btnEinzahlen)
        val btnAuszahlen: Button = findViewById(R.id.btnAuszahlen)

        val tableKonten: TableLayout = findViewById(R.id.tableKonten)

        // Tabellenkopf erstellen
        createTableHead(tableKonten)

        initialisiereKonten()
        aktualisiereTabelle()

        btnEinzahlen.setOnClickListener {
            aktuellesKonto?.let { konto ->
                val betrag = etBetrag.text.toString().toDoubleOrNull()
                betrag?.let { betragValue ->
                    konto.einzahlen(betragValue)
                    updateKontodetails(konto)
                }
                aktualisiereTabelle()
                Snackbar.make(btnEinzahlen, "Dies ist eine Snackbar", Snackbar.LENGTH_LONG).show()
            }
        }

        btnAuszahlen.setOnClickListener {
            aktuellesKonto?.let { konto ->
                val betrag = etBetrag.text.toString().toDoubleOrNull()
                betrag?.let { betragValue ->
                    if (konto.auszahlen(betragValue)) {
                        updateKontodetails(konto)
                    } else {
                        // Fehlerbehandlung, falls Auszahlung nicht möglich ist
                        // Zum Beispiel eine Toast-Nachricht anzeigen
                    }
                }
                aktualisiereTabelle()
            }
        }
    }

    private fun initialisiereKonten() {
        konten.apply {
            add(Girokonto(1000.0, 500.0))
            add(Girokonto(1500.0, 300.0))
            add(Girokonto(2000.0, 700.0))
            add(Girokonto(2500.0, 600.0))
            add(Girokonto(3000.0, 500.0))
        }
        aktualisiereTabelle()
    }

    private fun createTableHead(table: TableLayout) {
        val headRow = TableRow(this).apply {
            layoutParams = TableRow.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            background = createCellBackground(Color.DKGRAY) // Dunklerer Hintergrund für den Kopf
        }

        createCell("Konto", headRow, true)
        createCell("Kontostand", headRow, true)
        createCell("Dispo", headRow, true)

        table.addView(headRow)
    }

    private fun createCell(text: String, row: TableRow, isHead: Boolean) {
        val tv = TextView(this).apply {
            this.text = text
            layoutParams = TableRow.LayoutParams(0, WRAP_CONTENT, 1f).apply {
                setMargins(1, 1, 1, 1)
            }
            setPadding(8, 8, 8, 8)
            background = if (isHead) createCellBackground(Color.LTGRAY) else createCellBackground(Color.WHITE)
            setTypeface(null, if (isHead) Typeface.BOLD else Typeface.NORMAL)
            textAlignment = View.TEXT_ALIGNMENT_CENTER
        }
        row.addView(tv)
    }

    private fun createCellBackground(color: Int): GradientDrawable {
        return GradientDrawable().apply {
            shape = GradientDrawable.RECTANGLE
            setColor(color) // Farbe der Zelle
            setStroke(1, Color.BLACK) // Rahmen
        }
    }

    private fun updateKontodetails(konto: Girokonto) {
        tvKontostand.text = "Kontostand: ${konto.kontostand()}€"
        tvDispo.text = "Dispo: ${konto.getDispo()}€"
        aktuellesKonto = konto
    }

    private fun aktualisiereTabelle() {
        val tableKonten: TableLayout = findViewById(R.id.tableKonten)
        tableKonten.removeAllViews() // Entfernt alle bestehenden Zeilen

        // Kopfzeile hinzufügen, wie zuvor beschrieben

        konten.forEach { konto ->
            val row = TableRow(this).apply {
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, WRAP_CONTENT)
                isClickable = true
                background = ContextCompat.getDrawable(this@MainActivity, android.R.drawable.list_selector_background)
            }

            createCell("Konto", row, false)
            createCell("${konto.kontostand()}€", row, false)
            createCell("${konto.getDispo()}€", row, false)

            row.setOnClickListener {
                updateKontodetails(konto)
            }

            tableKonten.addView(row)
        }
    }

}